package com.hello.app.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/me")
public class TestController {
    @GetMapping("/{name}")
    public String showName(@PathVariable String name){
        return "Hello ".concat(name);
    }
}

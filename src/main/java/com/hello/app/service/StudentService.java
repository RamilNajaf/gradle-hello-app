package com.hello.app.service;

import com.hello.app.entity.Student;
import com.hello.app.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service
public class StudentService {
    private final StudentRepository studentRepository;

    public Student findStudentById(Long id){
        return studentRepository.findById(id).orElseThrow();
    }

}

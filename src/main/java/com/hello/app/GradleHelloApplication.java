package com.hello.app;

import com.hello.app.entity.Address;
import com.hello.app.entity.Faculty;
import com.hello.app.entity.Student;
import com.hello.app.entity.Teacher;
import com.hello.app.repository.AddressRepository;
import com.hello.app.repository.FacultyRepository;
import com.hello.app.repository.StudentRepository;
import com.hello.app.repository.TeacherRepository;
import liquibase.pro.packaged.T;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class GradleHelloApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final AddressRepository addressRepository;
    private final FacultyRepository facultyRepository;
    private final TeacherRepository teacherRepository;

    public static void main(String[] args) {
        SpringApplication.run(GradleHelloApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Address address = Address.builder()
                .country("AZE")
                .city("Baku")
                .street("Cefer Xendand")
                .build();
        addressRepository.save(address);


        Faculty faculty = Faculty.builder()
                .name("microservice")
                .build();
        facultyRepository.save(faculty);

        Teacher teacher = Teacher.builder()
                .name("Tofiq")
                .build();
        teacherRepository.save(teacher);



        Student student = studentRepository.findById(1l).orElseThrow();

        student.setAddress(address);
        student.setFaculty(faculty);
        student.setTeachers(Set.of(teacher));
        studentRepository.save(student);





    }
}
